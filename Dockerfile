FROM openjdk:11-jre-slim
VOLUME /tmp
ARG JAVA_OPTS`
ENV JAVA_OPTS=$JAVA_OPTS
COPY target/demo-0.0.1-SNAPSHOT.jar ${JAR_FILE}
EXPOSE 9603
ENTRYPOINT exec java $JAVA_OPTS -jar ${JAR_FILE}
# For Spring-Boot project, use the entrypoint below to reduce Tomcat startup time.
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar demo.jar
#target/demo-0.0.1-SNAPSHOT.jar
