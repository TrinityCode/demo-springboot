package com.trinitycode.demo.consumer;

import org.springframework.kafka.annotation.KafkaListener; 
import org.springframework.stereotype.Component;
 

@Component
public class KafkaConsumer {
    
    // @Autowired
    // ConcurrentKafkaListenerContainerFactory<String, String> concurrentKafkaListenerContainerFactory;
    
    @KafkaListener(topics="NewTopic", groupId = "group_id")
    public void consume(String message)
    {
        System.out.println("Kafka Message Received By Consumer Topic Class : " + message);
    }
}
