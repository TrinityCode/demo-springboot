package com.trinitycode.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer; 
import org.springframework.data.redis.serializer.StringRedisSerializer;
 

//@EnableRedisRepositories
@Configuration
public class RedisConfig {
    //KALO MAU CONFIGURE SENDIRI 
    @Value("${spring.redis.host}")
    private String redisHost;
    @Value("${spring.redis.port}")
    private int redisPort;
    @Bean
    public JedisConnectionFactory jedisConnectionFactory()
    {

        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(redisHost, redisPort);
        return new JedisConnectionFactory(redisStandaloneConfiguration);
    }
    @Bean
    public RedisTemplate<String, Object> redisTemplate()
    {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer() );
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        return template;

    }


    //KALO MAU AUTOCONFIGURE dari application.properties  

    //TODO "Not Working Yet"
    // @Bean
    // public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory)
    // {
    //     RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
    //     template.setConnectionFactory(connectionFactory);
    //     template.setKeySerializer(new StringRedisSerializer() );
    //     template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
    //     return template;

    // }
}
