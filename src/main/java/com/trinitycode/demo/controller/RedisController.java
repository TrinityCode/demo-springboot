package com.trinitycode.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/redis")
@RestController
public class RedisController {
    
    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @GetMapping("/init")
    public String init()
    {
       redisTemplate.boundValueOps("city").set("Bandung");
        return "redis initialized";
    }
    @GetMapping("/getcity")
    public String getCity()
    {
        String result ;
        result = redisTemplate.opsForValue().get("city").toString();
        return "redis retrieved " +  result;
    }
    


}

 