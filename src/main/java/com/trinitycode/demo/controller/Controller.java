package com.trinitycode.demo.controller;
import java.util.List;

import com.trinitycode.demo.config.MyConfig;
import com.trinitycode.demo.model.*;
import com.trinitycode.demo.repository.PersonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;


@RequestMapping("default")
@RestController
public class Controller {
    // public void Test (List<Person> people)
    // {

    // }
    @Autowired
    PersonRepository personRepository;

    @Autowired
    MyConfig myConfig;

    @GetMapping("/hello")
    public String hello()
    {
        String result = "Data 2222- ";
        return result + "Hello World222243";
    }
    @GetMapping("/init")
    public void init()
    {
        personRepository.save(new Person("Nama", "Satu"));
        personRepository.save(new Person("Nama", "Dua"));
        personRepository.save(new Person("Satu", "Dua"));
    }

    @GetMapping("/all")
    public List<Person> getAll (){
        return personRepository.findAll();
    }

    @GetMapping(value="/id")
    public Person getById(@RequestParam Long id) {
        return personRepository.findById(id).get();
        //return personRepository.getById(id); --> ini ga jalan nested exception
    }
    
    @GetMapping(value="/lastname")
    public List<Person> findByLastname(@RequestParam String lastName) {
        return personRepository.findByLastName(lastName);

    }
    
    @GetMapping(value="checkconfig")
    public String getMethodName() {
        String result = "";
        result = result + myConfig.getHost();
        result = result + myConfig.getPort();
        result = result + myConfig.getName();
        return result;
    }
    

    
   

}

// public class Person {
//     private String name;
//     public String getName(){
//         return name;
//     }

//     public void setName(string newName)
//     {
//         name = newName;
//     }
// }