package com.trinitycode.demo.controller;

import java.util.List;

import com.trinitycode.demo.model.Product;
import com.trinitycode.demo.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping("/all")
    public List<Product> getAll()
    {
        //test
        return productRepository.findAll();
    }

    @GetMapping("/new")
    public void addNew( String name, double price)
    {
        productRepository.save(new Product(name, price));
    }
    @GetMapping("/all/{order}")
    public List<Product> addNew(@PathVariable String order)
    {
        return order.equals("asc")  ? productRepository.findAllByOrderByNameAsc() : productRepository.findAllByOrderByNameDesc();

    }

    @GetMapping("/cheaper/{price}")
    public List<Product> getCheaper(@PathVariable double price)
    {
        return productRepository.findAllByPriceLessThan(price);
    }
}
