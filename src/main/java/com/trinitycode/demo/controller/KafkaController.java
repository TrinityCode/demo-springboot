package com.trinitycode.demo.controller; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("kafka")
@RestController
public class KafkaController {
    
    @Autowired
    KafkaTemplate<String, String> kafkaTemplate ;

    // @Autowired
    // KafkaConfig kafkaConfig;
    // @Autowired
    // ConcurrentKafkaListenerContainerFactory<String, String> concurrentKafkaListenerContainerFactory;

    @GetMapping("/send")
    public String sendMessage(String message)
    {
        // Properties properties = new Properties();
        // properties.put("bootstrap.servers", "207.148.124.246:9092");
        // properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        
        kafkaTemplate.send("NewTopic", message + " this ini message sent by Kafka Producer/Sender");
        // KafkaProducer<String,String> producer = new KafkaProducer<>(properties);
        // ProducerRecord<String,String> producerRecord = new ProducerRecord<String,String>("NewTopic", message);
        // producer.send(producerRecord);
        // producer.close();
        // producer.flush();

        return "published";


    }
}
