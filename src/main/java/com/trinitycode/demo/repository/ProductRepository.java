package com.trinitycode.demo.repository;

import java.util.List;

import com.trinitycode.demo.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long>{

    public Product findByName(String name);

    public List<Product> findAllByOrderByNameAsc();
    public List<Product> findAllByOrderByNameDesc();
    public List<Product> findAllByPriceLessThan(double price);
}
