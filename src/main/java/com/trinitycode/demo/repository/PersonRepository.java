package com.trinitycode.demo.repository;
import java.util.List;
import java.util.Optional;

import com.trinitycode.demo.model.Person;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Long> {
    
    List<Person> findByLastName(String lastName);

    Optional<Person> findById(Long id);
    
    Person getById(Long id);
}
